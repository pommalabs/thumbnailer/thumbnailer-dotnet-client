# Thumbnailer Client for .NET

[![License: MIT][project-license-badge]][project-license]
[![Donate][paypal-donations-badge]][paypal-donations]
[![Docs][docfx-docs-badge]][docfx-docs]
[![NuGet version][nuget-version-badge]][nuget-package]
[![NuGet downloads][nuget-downloads-badge]][nuget-package]

[![standard-readme compliant][github-standard-readme-badge]][github-standard-readme]
[![GitLab pipeline status][gitlab-pipeline-status-badge]][gitlab-pipelines]
[![Quality gate][sonar-quality-gate-badge]][sonar-website]
[![Code coverage][sonar-coverage-badge]][sonar-website]
[![Renovate enabled][renovate-badge]][renovate-website]

A .NET client for [Thumbnailer][gitlab-thumbnailer], which wraps
[v2 endpoints][thumbnailer-swagger-v2] under an easy to use interface.

## Table of Contents

- [Install](#install)
- [Usage](#usage)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
  - [Contributors](#contributors)
  - [Editing](#editing)
  - [Restoring dependencies](#restoring-dependencies)
  - [Running tests](#running-tests)
- [License](#license)

## Install

NuGet package [PommaLabs.Thumbnailer.Client][nuget-package] is available for download:

```bash
dotnet add package PommaLabs.Thumbnailer.Client
```

## Usage

Client wraps the job-based logic of [v2 endpoints][thumbnailer-swagger-v2]
under a simpler interface, which hides the details of Thumbnailer jobs and exposes
high-level operations such as media optimization and thumbnail generation.

When using dependency injection, client can be registered
and [configured][thumbnailer-client-configuration] using following extension method:

```cs
using Microsoft.Extensions.DependencyInjection;
using PommaLabs.Thumbnailer.Client;

services.Configure<ThumbnailerClientConfiguration>(Configuration.GetSection("Thumbnailer"));
services.AddThumbnailerClient();
```

After that, client will be available as an instance of [`IThumbnailerClient`][thumbnailer-client-interface].
Client can also be created by instancing following class, providing required objects
such as [client configuration][thumbnailer-client-configuration] and a logger:

```cs
using PommaLabs.Thumbnailer.Client;
using PommaLabs.Thumbnailer.Client.Services.Clients;

IThumbnailerClient thumbnailerClient = new ConcreteThumbnailerClient(clientConfiguration, logger);
```

**Media optimization** can be invoked with following methods:

```cs
var optimizedMediaBytes = await thumbnailerClient.OptimizeMediaAsync(
  fileContents, optionalContentType, optionalFileName, optionalCancellationToken);

var optimizedMediaBytes = await thumbnailerClient.OptimizeMediaAsync(
  fileUri, optionalCancellationToken);
```

**Thumbnail generation** can be invoked with following methods:

```cs
var thumbnailBytes = await thumbnailerClient.GenerateThumbnailAsync(
  fileContents, optionalContentType, optionalFileName,
  /* Thumbnail generation parameters... */, optionalCancellationToken);

var thumbnailBytes = await thumbnailerClient.GenerateThumbnailAsync(
  fileUri, /* Thumbnail generation parameters... */, optionalCancellationToken);
```

## Maintainers

[@pomma89][gitlab-pomma89].

## Contributing

MRs accepted.

Small note: If editing the README, please conform to the [standard-readme][github-standard-readme] specification.

### Contributors

@AlessioGiacobbe implemented new client methods to access image placeholder generation endpoints,
which rely on [BlurHash][blurhash-website] and [ThumbHash][thumbhash-website] algorithms.

### Editing

[Visual Studio Code][vscode-website], with [Remote Containers extension][vscode-remote-containers],
is the recommended way to work on this project.

A development container has been configured with all required tools.

[Visual Studio Community][vs-website] is also supported
and an updated solution file, `thumbnailer-dotnet-client.sln`, has been provided.

### Restoring dependencies

When opening the development container, dependencies should be automatically restored.

Anyway, dependencies can be restored with following command:

```bash
dotnet restore
```

### Running tests

Tests can be run with following command:

```bash
dotnet test
```

Tests can also be run with following command, which collects coverage information:

```bash
./build.sh --target run-tests
```

## License

MIT © 2019-2024 [PommaLabs Team and Contributors][pommalabs-website]

[blurhash-website]: https://blurha.sh/
[docfx-docs]: https://thumbnailer-dotnet-client-docs.pommalabs.xyz/
[docfx-docs-badge]: https://img.shields.io/badge/DocFX-OK-green?style=flat-square
[github-standard-readme]: https://github.com/RichardLitt/standard-readme
[github-standard-readme-badge]: https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square
[gitlab-pipeline-status-badge]: https://gitlab.com/pommalabs/thumbnailer/thumbnailer-dotnet-client/badges/main/pipeline.svg?style=flat-square
[gitlab-pipelines]: https://gitlab.com/pommalabs/thumbnailer/thumbnailer-dotnet-client/pipelines
[gitlab-pomma89]: https://gitlab.com/pomma89
[gitlab-thumbnailer]: https://gitlab.com/pommalabs/thumbnailer/thumbnailer
[nuget-downloads-badge]: https://img.shields.io/nuget/dt/PommaLabs.Thumbnailer.Client?style=flat-square
[nuget-package]: https://www.nuget.org/packages/PommaLabs.Thumbnailer.Client/
[nuget-version-badge]: https://img.shields.io/nuget/v/PommaLabs.Thumbnailer.Client?style=flat-square
[paypal-donations]: https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=ELJWKEYS9QGKA
[paypal-donations-badge]: https://img.shields.io/badge/Donate-PayPal-important.svg?style=flat-square
[pommalabs-website]: https://pommalabs.xyz/
[project-license]: https://gitlab.com/pommalabs/thumbnailer/thumbnailer-dotnet-client/-/blob/main/LICENSE
[project-license-badge]: https://img.shields.io/badge/License-MIT-yellow.svg?style=flat-square
[renovate-badge]: https://img.shields.io/badge/renovate-enabled-brightgreen.svg?style=flat-square
[renovate-website]: https://renovate.whitesourcesoftware.com/
[sonar-coverage-badge]: https://img.shields.io/sonar/coverage/pommalabs_thumbnailer-dotnet-client?server=https%3A%2F%2Fsonarcloud.io&sonarVersion=8&style=flat-square
[sonar-quality-gate-badge]: https://img.shields.io/sonar/quality_gate/pommalabs_thumbnailer-dotnet-client?server=https%3A%2F%2Fsonarcloud.io&sonarVersion=8&style=flat-square
[sonar-website]: https://sonarcloud.io/dashboard?id=pommalabs_thumbnailer-dotnet-client
[thumbhash-website]: https://evanw.github.io/thumbhash/
[thumbnailer-swagger-v2]: https://thumbnailer.pommalabs.xyz/swagger/index.html?urls.primaryName=V2
[thumbnailer-client-configuration]: https://thumbnailer-dotnet-client-docs.pommalabs.xyz/api/PommaLabs.Thumbnailer.Client.ThumbnailerClientConfiguration.html
[thumbnailer-client-interface]: https://thumbnailer-dotnet-client-docs.pommalabs.xyz/api/PommaLabs.Thumbnailer.Client.IThumbnailerClient.html
[vs-website]: https://visualstudio.microsoft.com/
[vscode-remote-containers]: https://code.visualstudio.com/docs/remote/containers
[vscode-website]: https://code.visualstudio.com/
