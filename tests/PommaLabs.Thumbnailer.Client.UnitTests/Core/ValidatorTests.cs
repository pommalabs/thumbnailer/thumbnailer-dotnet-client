﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using Microsoft.Extensions.Logging.Abstractions;
using NUnit.Framework;
using PommaLabs.MimeTypes;
using PommaLabs.Thumbnailer.Client.Core;

namespace PommaLabs.Thumbnailer.Client.UnitTests.Core;

[TestFixture, Parallelizable]
internal sealed class ValidatorTests
{
    #region ValidateContentTypeFor*

    [Test]
    public void ValidateContentTypeForMediaOptimization_WithInvalidContentType_ShouldThrowNotSupportedException()
    {
        // Arrange
        var validator = new Validator(NullLogger.Instance);

        // Act & Assert
        Assert.That(
            () => validator.ValidateContentTypeForMediaOptimization(MimeTypeMap.APPLICATION.OCTET_STREAM, @throw: true),
            Throws.TypeOf<NotSupportedException>().With.Message.Contains("media optimization"));
    }

    [Test]
    public void ValidateContentTypeForThumbnailGeneration_WithInvalidContentType_ShouldThrowNotSupportedException()
    {
        // Arrange
        var validator = new Validator(NullLogger.Instance);

        // Act & Assert
        Assert.That(
            () => validator.ValidateContentTypeForThumbnailGeneration(MimeTypeMap.APPLICATION.OCTET_STREAM, @throw: true),
            Throws.TypeOf<NotSupportedException>().With.Message.Contains("thumbnail generation"));
    }

    [Test]
    public void ValidateContentTypeForImagePlaceholderGeneration_WithInvalidContentType_ShouldThrowNotSupportedException()
    {
        // Arrange
        var validator = new Validator(NullLogger.Instance);

        // Act & Assert
        Assert.That(
            () => validator.ValidateContentTypeForImagePlaceholderGeneration(MimeTypeMap.APPLICATION.OCTET_STREAM, @throw: true),
            Throws.TypeOf<NotSupportedException>().With.Message.Contains("image placeholder generation"));
    }

    #endregion

    #region ValidateFileBytes

    [Test]
    public void ValidateFileBytes_NullBytes_ShouldThrowArgumentNullException()
    {
        // Arrange
        var validator = new Validator(NullLogger.Instance);
        byte[] fileBytes = null!;

        // Act & Assert
        Assert.That(
            () => validator.ValidateFileBytes(fileBytes),
            Throws.TypeOf<ArgumentNullException>().With.Message.Contains("null"));
    }

    [Test]
    public void ValidateFileBytes_EmptyBytes_ShouldNotThrow()
    {
        // Arrange
        var validator = new Validator(NullLogger.Instance);
        byte[] fileBytes = [];

        // Act & Assert
        Assert.DoesNotThrow(() => validator.ValidateFileBytes(fileBytes));
    }

    #endregion

    #region ValidateFileUri

    [Test]
    public void ValidateFileUri_WithRelativeUri_ShouldThrowNotSupportedException()
    {
        // Arrange
        var validator = new Validator(NullLogger.Instance);
        var fileUri = new Uri("/relative/file/uri", UriKind.Relative);

        // Act & Assert
        Assert.That(
            () => validator.ValidateFileUri(fileUri),
            Throws.TypeOf<NotSupportedException>().With.Message.Contains("Relative"));
    }

    [Test]
    public void ValidateFileUri_WithInvalidScheme_ShouldThrowNotSupportedException()
    {
        // Arrange
        var validator = new Validator(NullLogger.Instance);
        var fileUri = new Uri("file:///absolute/file/uri", UriKind.Absolute);

        // Act & Assert
        Assert.That(
            () => validator.ValidateFileUri(fileUri),
            Throws.TypeOf<NotSupportedException>().With.Message.Contains("http/https"));
    }

    [TestCase("http://localhost/my/file")]
    [TestCase("http://127.0.0.1/my/file")]
    public void ValidateFileUri_WithLoopbackUri_ShouldThrowNotSupportedException(string loopbackUri)
    {
        // Arrange
        var validator = new Validator(NullLogger.Instance);
        var fileUri = new Uri(loopbackUri, UriKind.Absolute);

        // Act & Assert
        Assert.That(
            () => validator.ValidateFileUri(fileUri),
            Throws.TypeOf<NotSupportedException>().With.Message.Contains("Loopback"));
    }

    #endregion
}
