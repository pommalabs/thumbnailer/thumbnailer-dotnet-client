﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System.Threading;
using System.Threading.Tasks;
using FakeItEasy;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using NUnit.Framework;
using PommaLabs.Thumbnailer.Client.Core;

namespace PommaLabs.Thumbnailer.Client.UnitTests;

[TestFixture, Parallelizable(ParallelScope.Fixtures)]
internal sealed class ThumbnailerHealthCheckTests
{
    #region Setup/Teardown

    [SetUp]
    public void SetUp()
    {
        HealthCheckCacheHelper<IThumbnailerClient>.ClearCachedResults();
    }

    [TearDown]
    public void TearDown()
    {
        HealthCheckCacheHelper<IThumbnailerClient>.ClearCachedResults();
    }

    #endregion Setup/Teardown

    [Test]
    public async Task ShouldReuseCachedResult_WhenHealthCheckFails_AndResultIsStillValid()
    {
        // Arrange
        var client = A.Fake<IThumbnailerClient>();
        A.CallTo(() => client.IsHealthyAsync(A<CancellationToken>._)).Returns(false);

        var healthCheck = new ThumbnailerHealthCheck(client);

        var context = new HealthCheckContext();
        await healthCheck.CheckHealthAsync(context);

        // Act
        await healthCheck.CheckHealthAsync(context);

        // Assert
        A.CallTo(() => client.IsHealthyAsync(A<CancellationToken>._))
            .MustHaveHappenedOnceExactly();
    }

    [Test]
    public async Task ShouldReuseCachedResult_WhenHealthCheckSucceeds_AndResultIsStillValid()
    {
        // Arrange
        var client = A.Fake<IThumbnailerClient>();
        A.CallTo(() => client.IsHealthyAsync(A<CancellationToken>._)).Returns(true);

        var healthCheck = new ThumbnailerHealthCheck(client);

        var context = new HealthCheckContext();
        await healthCheck.CheckHealthAsync(context);

        // Act
        await healthCheck.CheckHealthAsync(context);

        // Assert
        A.CallTo(() => client.IsHealthyAsync(A<CancellationToken>._))
            .MustHaveHappenedOnceExactly();
    }
}
