﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using PommaLabs.Thumbnailer.Client.Core;
using PommaLabs.Thumbnailer.Client.Models.Results;
using ImagePlaceholderAlgorithm = PommaLabs.Thumbnailer.Client.Models.Enumerations.ImagePlaceholderAlgorithm;
using ImagePlaceholderResult = PommaLabs.Thumbnailer.Client.Models.Results.ImagePlaceholderResult;

[assembly: InternalsVisibleTo("PommaLabs.Thumbnailer.Client.UnitTests")]
[assembly: InternalsVisibleTo("PommaLabs.Thumbnailer.IntegrationTests")]

namespace PommaLabs.Thumbnailer.Client;

/// <summary>
///   Thumbnailer client.
/// </summary>
public interface IThumbnailerClient
{
    /// <summary>
    ///   Generates a placeholder of given image.
    /// </summary>
    /// <param name="fileUri">Source file URI.</param>
    /// <param name="imagePlaceholderAlgorithm">Desired placeholder generation algorithm.</param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <returns>A very compact representation of given source image.</returns>
    Task<ImagePlaceholderResult> GenerateImagePlaceholderAsync(
        Uri fileUri,
        ImagePlaceholderAlgorithm imagePlaceholderAlgorithm,
        CancellationToken cancellationToken = default);

    /// <summary>
    ///   Generates a placeholder of given image.
    /// </summary>
    /// <param name="contents">File bytes.</param>
    /// <param name="imagePlaceholderAlgorithm">Desired placeholder generation algorithm.</param>
    /// <param name="contentType">
    ///   File content type. If not specified, it will be deduced from file signature: however,
    ///   please specify it if available.
    /// </param>
    /// <param name="fileName">
    ///   File name. If not specified, its value will be a random file name compatible with
    ///   specified (or deduced) content type.
    /// </param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <returns>A very compact representation of given source image.</returns>
    Task<ImagePlaceholderResult> GenerateImagePlaceholderAsync(
        byte[] contents,
        ImagePlaceholderAlgorithm imagePlaceholderAlgorithm,
        string? contentType = default,
        string? fileName = default,
        CancellationToken cancellationToken = default);

    /// <summary>
    ///   Produces a thumbnail of given source file.
    /// </summary>
    /// <param name="fileUri">Source file URI.</param>
    /// <param name="widthPx">
    ///   Max thumbnail width. Thumbnail will be generated preserving the source file aspect ratio
    ///   and, if <paramref name="fill"/> is true, a transparent background will be added so that
    ///   thumbnail width and height are exactly as requested.
    /// </param>
    /// <param name="heightPx">
    ///   Max thumbnail height. Thumbnail will be generated preserving the source file aspect ratio
    ///   and, if <paramref name="fill"/> is true, a transparent background will be added so that
    ///   thumbnail width and height are exactly as requested.
    /// </param>
    /// <param name="shavePx">Shaves <paramref name="shavePx"/> from source file edges.</param>
    /// <param name="fill">
    ///   If true, a transparent background will be added so that thumbnail width and height are
    ///   exactly as requested, preserving source file aspect ratio. When <paramref
    ///   name="smartCrop"/> is enabled, this parameter will be ignored. Defaults to true.
    /// </param>
    /// <param name="smartCrop">
    ///   Finds a good crop which satisfies specified <paramref name="widthPx"/> and <paramref
    ///   name="heightPx"/> parameters. When smart crop is enabled, <paramref name="fill"/> is
    ///   ignored. Defaults to false.
    /// </param>
    /// <param name="fallback">
    ///   If true, a fallback image is generated when thumbnail generation fails. Fallback image is
    ///   generated starting from an SVG template, whose customization is documented in project
    ///   README. Defaults to false.
    /// </param>
    /// <param name="imagePlaceholderAlgorithm">
    ///   Desired placeholder generation algorithm. Defaults to <see cref="ImagePlaceholderAlgorithm.None"/>.
    /// </param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <returns>A thumbnail of given source file.</returns>
    Task<FileResult> GenerateThumbnailAsync(
        Uri fileUri,
        ushort widthPx = Validator.ThumbnailSidePx,
        ushort heightPx = Validator.ThumbnailSidePx,
        ushort shavePx = Validator.ThumbnailShavePx,
        bool fill = true,
        bool smartCrop = false,
        bool fallback = false,
        ImagePlaceholderAlgorithm imagePlaceholderAlgorithm = ImagePlaceholderAlgorithm.None,
        CancellationToken cancellationToken = default);

    /// <summary>
    ///   Produces a thumbnail of given source file.
    /// </summary>
    /// <param name="contents">File bytes.</param>
    /// <param name="contentType">
    ///   File content type. If not specified, it will be deduced from file signature: however,
    ///   please specify it if available.
    /// </param>
    /// <param name="fileName">
    ///   File name. If not specified, its value will be a random file name compatible with
    ///   specified (or deduced) content type.
    /// </param>
    /// <param name="widthPx">
    ///   Max thumbnail width. Thumbnail will be generated preserving the source file aspect ratio
    ///   and, if <paramref name="fill"/> is true, a transparent background will be added so that
    ///   thumbnail width and height are exactly as requested.
    /// </param>
    /// <param name="heightPx">
    ///   Max thumbnail height. Thumbnail will be generated preserving the source file aspect ratio
    ///   and, if <paramref name="fill"/> is true, a transparent background will be added so that
    ///   thumbnail width and height are exactly as requested.
    /// </param>
    /// <param name="shavePx">Shaves <paramref name="shavePx"/> from source file edges.</param>
    /// <param name="fill">
    ///   If true, a transparent background will be added so that thumbnail width and height are
    ///   exactly as requested, preserving source file aspect ratio. When <paramref
    ///   name="smartCrop"/> is enabled, this parameter will be ignored. Defaults to true.
    /// </param>
    /// <param name="smartCrop">
    ///   Finds a good crop which satisfies specified <paramref name="widthPx"/> and <paramref
    ///   name="heightPx"/> parameters. When smart crop is enabled, <paramref name="fill"/> is
    ///   ignored. Defaults to false.
    /// </param>
    /// <param name="fallback">
    ///   If true, a fallback image is generated when thumbnail generation fails. Fallback image is
    ///   generated starting from an SVG template, whose customization is documented in project
    ///   README. Defaults to false.
    /// </param>
    /// <param name="imagePlaceholderAlgorithm">
    ///   Desired placeholder generation algorithm. Defaults to <see cref="ImagePlaceholderAlgorithm.None"/>.
    /// </param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <returns>A thumbnail of given source file.</returns>
    Task<FileResult> GenerateThumbnailAsync(
        byte[] contents,
        string? contentType = default,
        string? fileName = default,
        ushort widthPx = Validator.ThumbnailSidePx,
        ushort heightPx = Validator.ThumbnailSidePx,
        ushort shavePx = Validator.ThumbnailShavePx,
        bool fill = true,
        bool smartCrop = false,
        bool fallback = false,
        ImagePlaceholderAlgorithm imagePlaceholderAlgorithm = ImagePlaceholderAlgorithm.None,
        CancellationToken cancellationToken = default);

    /// <summary>
    ///   Checks whether Thumbnailer service is healthy or not.
    /// </summary>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <returns>True if service is healthy, false otherwise.</returns>
    Task<bool> IsHealthyAsync(CancellationToken cancellationToken = default);

    /// <summary>
    ///   Returns true if given content type is supported by image placeholder generation, false otherwise.
    /// </summary>
    /// <param name="contentType">Content type.</param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <returns>
    ///   True if given content type is supported by image placeholder generation, false otherwise.
    /// </returns>
    Task<bool> IsImagePlaceholderGenerationSupportedAsync(string contentType, CancellationToken cancellationToken = default);

    /// <summary>
    ///   Returns true if given content type is supported by media file optimization with specified
    ///   mode, false otherwise.
    /// </summary>
    /// <param name="contentType">Content type.</param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <returns>
    ///   True if given content type is supported by media file optimization, false otherwise.
    /// </returns>
    Task<bool> IsMediaOptimizationSupportedAsync(string contentType, CancellationToken cancellationToken = default);

    /// <summary>
    ///   Returns true if given content type is supported by thumbnail generation, false otherwise.
    /// </summary>
    /// <param name="contentType">Content type.</param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <returns>True if given content type is supported by thumbnail generation, false otherwise.</returns>
    Task<bool> IsThumbnailGenerationSupportedAsync(string contentType, CancellationToken cancellationToken = default);

    /// <summary>
    ///   Optimizes given source media file.
    /// </summary>
    /// <param name="fileUri">Source file URI.</param>
    /// <param name="imagePlaceholderAlgorithm">
    ///   Desired placeholder generation algorithm. Defaults to <see cref="ImagePlaceholderAlgorithm.None"/>.
    /// </param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <returns>An optimized version of given source media file.</returns>
    Task<FileResult> OptimizeMediaAsync(
        Uri fileUri,
        ImagePlaceholderAlgorithm imagePlaceholderAlgorithm = ImagePlaceholderAlgorithm.None,
        CancellationToken cancellationToken = default);

    /// <summary>
    ///   Optimizes given source media file.
    /// </summary>
    /// <param name="contents">File bytes.</param>
    /// <param name="contentType">
    ///   File content type. If not specified, it will be deduced from file signature: however,
    ///   please specify it if available.
    /// </param>
    /// <param name="fileName">
    ///   File name. If not specified, its value will be a random file name compatible with
    ///   specified (or deduced) content type.
    /// </param>
    /// <param name="imagePlaceholderAlgorithm">
    ///   Desired placeholder generation algorithm. Defaults to <see cref="ImagePlaceholderAlgorithm.None"/>.
    /// </param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <returns>An optimized version of given source media file.</returns>
    Task<FileResult> OptimizeMediaAsync(
        byte[] contents,
        string? contentType = default,
        string? fileName = default,
        ImagePlaceholderAlgorithm imagePlaceholderAlgorithm = ImagePlaceholderAlgorithm.None,
        CancellationToken cancellationToken = default);
}
