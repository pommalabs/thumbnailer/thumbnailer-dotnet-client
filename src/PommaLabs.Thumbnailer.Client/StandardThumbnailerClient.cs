﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Polly;
using Polly.Retry;
using PommaLabs.MimeTypes;
using PommaLabs.Thumbnailer.Client.Core;
using PommaLabs.Thumbnailer.Client.Models.Enumerations;
using PommaLabs.Thumbnailer.Client.Models.Results;
using PommaLabs.Thumbnailer.Client.Resources;
using ImagePlaceholderAlgorithm = PommaLabs.Thumbnailer.Client.Models.Enumerations.ImagePlaceholderAlgorithm;
using ImagePlaceholderResult = PommaLabs.Thumbnailer.Client.Models.Results.ImagePlaceholderResult;

namespace PommaLabs.Thumbnailer.Client;

/// <summary>
///   Standard Thumbnailer client.
/// </summary>
public sealed class StandardThumbnailerClient : IThumbnailerClient, IDisposable
{
    private const string HealthCheckEndpoint = "health";
    private const string JobsEndpoint = "api/v2/jobs";
    private const string MediaOptimizationEndpoint = "api/v2/optimize";
    private const string ThumbnailGenerationEndpoint = "api/v2/thumbnail";
    private const string ImagePlaceholderGenerationEndpoint = "api/v2/imagePlaceholder";

    private static readonly HashSet<int> s_statusCodesOfTransientErrors = [424, 429];

    private readonly IOptions<ThumbnailerClientConfiguration> _clientConfiguration;
    private readonly HttpClient _httpClient;
    private readonly ResiliencePipeline _resiliencePipeline;
    private readonly Validator _validator;
    private bool _disposedValue;

    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="clientConfiguration">Thumbnailer client configuration.</param>
    /// <param name="logger">Logger.</param>
    public StandardThumbnailerClient(
        IOptions<ThumbnailerClientConfiguration> clientConfiguration,
        ILogger<StandardThumbnailerClient> logger)
    {
        _clientConfiguration = clientConfiguration;
        _httpClient = HttpClientFactory.CreateHttpClient(clientConfiguration);
        _validator = new Validator(logger);

        // Wait 2 seconds after first error, then 4, 8, etc.
        _resiliencePipeline = new ResiliencePipelineBuilder()
            .AddRetry(new RetryStrategyOptions
            {
                ShouldHandle = new PredicateBuilder()
                    .Handle<ApiException>(ex => s_statusCodesOfTransientErrors.Contains(ex.StatusCode))
                    .Handle<TaskCanceledException>(ex => ex.InnerException is TimeoutException),
                MaxRetryAttempts = clientConfiguration.Value.RetryCount,
                DelayGenerator = static args => new ValueTask<TimeSpan?>(TimeSpan.FromSeconds(Math.Pow(2, args.AttemptNumber + 1)))
            })
            .Build();
    }

    /// <inheritdoc/>
    public async Task<ImagePlaceholderResult> GenerateImagePlaceholderAsync(Uri fileUri, ImagePlaceholderAlgorithm imagePlaceholderAlgorithm, CancellationToken cancellationToken = default)
    {
        _validator.ValidateFileUri(fileUri);

        var endpoint = UrlCombine(_clientConfiguration.Value.BaseUri.AbsoluteUri, ImagePlaceholderGenerationEndpoint);
        using (StartClientActivity(ActivityNames.ImagePlaceholderGeneration, HttpMethod.Get, endpoint))
        {
            var apiClient = new ImagePlaceholderGenerationApiClient(_httpClient) { ApiKey = _clientConfiguration.Value.ApiKey };
            var job = await EvaluateResponseAsync(OperationType.ImagePlaceholderGeneration, async () =>
                await apiClient.GenerateImagePlaceholderFromUriV2Async(fileUri, MapImagePlaceholderAlgorithm(imagePlaceholderAlgorithm), cancellationToken).ConfigureAwait(false),
                cancellationToken).ConfigureAwait(false);

            await CheckJobStatusAsync(job, cancellationToken).ConfigureAwait(false);
            return await DownloadImagePlaceholderResultAsync(job, cancellationToken).ConfigureAwait(false);
        }
    }

    /// <inheritdoc/>
    public async Task<ImagePlaceholderResult> GenerateImagePlaceholderAsync(byte[] contents, ImagePlaceholderAlgorithm imagePlaceholderAlgorithm, string? contentType = null, string? fileName = null, CancellationToken cancellationToken = default)
    {
        if (contentType == null)
        {
            (contentType, fileName) = MimeTypeHelper.GetMimeTypeAndExtension(contents, fileName);
        }

        _validator.ValidateFileBytes(contents);
        _validator.ValidateContentTypeForMediaOptimization(contentType, @throw: true);

        fileName = AdjustFileName(contentType, fileName);

        var endpoint = UrlCombine(_clientConfiguration.Value.BaseUri.AbsoluteUri, ImagePlaceholderGenerationEndpoint);
        using (StartClientActivity(ActivityNames.ImagePlaceholderGeneration, HttpMethod.Post, endpoint))
        {
            var file = new FileParameter(new MemoryStream(contents), fileName, contentType);

            var apiClient = new ImagePlaceholderGenerationApiClient(_httpClient) { ApiKey = _clientConfiguration.Value.ApiKey };
            var job = await EvaluateResponseAsync(OperationType.ImagePlaceholderGeneration, async () =>
                await apiClient.GenerateImagePlaceholderFromFileV2Async(MapImagePlaceholderAlgorithm(imagePlaceholderAlgorithm), file, cancellationToken).ConfigureAwait(false),
                cancellationToken).ConfigureAwait(false);

            await CheckJobStatusAsync(job, cancellationToken).ConfigureAwait(false);
            return await DownloadImagePlaceholderResultAsync(job, cancellationToken).ConfigureAwait(false);
        }
    }

    /// <inheritdoc/>
    public async Task<FileResult> GenerateThumbnailAsync(
        Uri fileUri,
        ushort widthPx = Validator.ThumbnailSidePx,
        ushort heightPx = Validator.ThumbnailSidePx,
        ushort shavePx = Validator.ThumbnailShavePx,
        bool fill = true,
        bool smartCrop = false,
        bool fallback = false,
        ImagePlaceholderAlgorithm imagePlaceholderAlgorithm = ImagePlaceholderAlgorithm.None,
        CancellationToken cancellationToken = default)
    {
        _validator.ValidateFileUri(fileUri);

        var endpoint = UrlCombine(_clientConfiguration.Value.BaseUri.AbsoluteUri, ThumbnailGenerationEndpoint);
        using (StartClientActivity(ActivityNames.ThumbnailGeneration, HttpMethod.Get, endpoint))
        {
            var apiClient = new ThumbnailGenerationApiClient(_httpClient) { ApiKey = _clientConfiguration.Value.ApiKey };
            var job = await EvaluateResponseAsync(OperationType.ThumbnailGeneration, async () =>
                await apiClient.GenerateThumbnailFromUriV2Async(fileUri, widthPx, heightPx, shavePx, fill, smartCrop, fallback, MapImagePlaceholderAlgorithm(imagePlaceholderAlgorithm), cancellationToken).ConfigureAwait(false),
                cancellationToken).ConfigureAwait(false);

            await CheckJobStatusAsync(job, cancellationToken).ConfigureAwait(false);
            return await DownloadFileResultAsync(job, cancellationToken).ConfigureAwait(false);
        }
    }

    /// <inheritdoc/>
    public async Task<FileResult> GenerateThumbnailAsync(
        byte[] contents,
        string? contentType = default,
        string? fileName = default,
        ushort widthPx = Validator.ThumbnailSidePx,
        ushort heightPx = Validator.ThumbnailSidePx,
        ushort shavePx = Validator.ThumbnailShavePx,
        bool fill = true,
        bool smartCrop = false,
        bool fallback = false,
        ImagePlaceholderAlgorithm imagePlaceholderAlgorithm = ImagePlaceholderAlgorithm.None,
        CancellationToken cancellationToken = default)
    {
        if (contentType == null)
        {
            (contentType, fileName) = MimeTypeHelper.GetMimeTypeAndExtension(contents, fileName);
        }

        _validator.ValidateFileBytes(contents);
        try
        {
            _validator.ValidateContentTypeForThumbnailGeneration(contentType, @throw: true);
        }
        catch (NotSupportedException) when (fallback)
        {
            // When fallback is enabled, thumbnail generation should not be blocked if content type
            // is not valid. A fallback image will be generated server side.
        }

        fileName = AdjustFileName(contentType, fileName);

        var endpoint = UrlCombine(_clientConfiguration.Value.BaseUri.AbsoluteUri, ThumbnailGenerationEndpoint);
        using (StartClientActivity(ActivityNames.ThumbnailGeneration, HttpMethod.Post, endpoint))
        {
            var file = new FileParameter(new MemoryStream(contents), fileName, contentType);

            var apiClient = new ThumbnailGenerationApiClient(_httpClient) { ApiKey = _clientConfiguration.Value.ApiKey };
            var job = await EvaluateResponseAsync(OperationType.ThumbnailGeneration, async () =>
                await apiClient.GenerateThumbnailFromFileV2Async(widthPx, heightPx, shavePx, fill, smartCrop, fallback, MapImagePlaceholderAlgorithm(imagePlaceholderAlgorithm), file, cancellationToken).ConfigureAwait(false),
                cancellationToken).ConfigureAwait(false);

            await CheckJobStatusAsync(job, cancellationToken).ConfigureAwait(false);
            return await DownloadFileResultAsync(job, cancellationToken).ConfigureAwait(false);
        }
    }

    /// <inheritdoc/>
    public async Task<bool> IsHealthyAsync(CancellationToken cancellationToken = default)
    {
        var endpoint = UrlCombine(_clientConfiguration.Value.BaseUri.AbsoluteUri, HealthCheckEndpoint);
        using (StartClientActivity(ActivityNames.HealthCheck, HttpMethod.Get, endpoint))
        {
            var response = await _httpClient.GetAsync(HealthCheckEndpoint, cancellationToken);
            return response.StatusCode == HttpStatusCode.OK;
        }
    }

    /// <inheritdoc/>
    public Task<bool> IsImagePlaceholderGenerationSupportedAsync(string contentType, CancellationToken cancellationToken = default)
    {
        return Task.FromResult(_validator.ValidateContentTypeForImagePlaceholderGeneration(contentType, @throw: false));
    }

    /// <inheritdoc/>
    public Task<bool> IsMediaOptimizationSupportedAsync(string contentType, CancellationToken cancellationToken = default)
    {
        return Task.FromResult(_validator.ValidateContentTypeForMediaOptimization(contentType, @throw: false));
    }

    /// <inheritdoc/>
    public Task<bool> IsThumbnailGenerationSupportedAsync(string contentType, CancellationToken cancellationToken = default)
    {
        return Task.FromResult(_validator.ValidateContentTypeForThumbnailGeneration(contentType, @throw: false));
    }

    /// <inheritdoc/>
    public async Task<FileResult> OptimizeMediaAsync(
        Uri fileUri,
        ImagePlaceholderAlgorithm imagePlaceholderAlgorithm = ImagePlaceholderAlgorithm.None,
        CancellationToken cancellationToken = default)
    {
        _validator.ValidateFileUri(fileUri);

        var endpoint = UrlCombine(_clientConfiguration.Value.BaseUri.AbsoluteUri, MediaOptimizationEndpoint);
        using (StartClientActivity(ActivityNames.MediaOptimization, HttpMethod.Get, endpoint))
        {
            var apiClient = new MediaOptimizationApiClient(_httpClient) { ApiKey = _clientConfiguration.Value.ApiKey };
            var job = await EvaluateResponseAsync(OperationType.MediaOptimization, async () =>
                await apiClient.OptimizeMediaFromUriV2Async(fileUri, MapImagePlaceholderAlgorithm(imagePlaceholderAlgorithm), cancellationToken).ConfigureAwait(false),
                cancellationToken).ConfigureAwait(false);

            await CheckJobStatusAsync(job, cancellationToken).ConfigureAwait(false);
            return await DownloadFileResultAsync(job, cancellationToken).ConfigureAwait(false);
        }
    }

    /// <inheritdoc/>
    public async Task<FileResult> OptimizeMediaAsync(
        byte[] contents,
        string? contentType = default,
        string? fileName = default,
        ImagePlaceholderAlgorithm imagePlaceholderAlgorithm = ImagePlaceholderAlgorithm.None,
        CancellationToken cancellationToken = default)
    {
        if (contentType == null)
        {
            (contentType, fileName) = MimeTypeHelper.GetMimeTypeAndExtension(contents, fileName);
        }

        _validator.ValidateFileBytes(contents);
        _validator.ValidateContentTypeForMediaOptimization(contentType, @throw: true);

        fileName = AdjustFileName(contentType, fileName);

        var endpoint = UrlCombine(_clientConfiguration.Value.BaseUri.AbsoluteUri, MediaOptimizationEndpoint);
        using (StartClientActivity(ActivityNames.MediaOptimization, HttpMethod.Post, endpoint))
        {
            var file = new FileParameter(new MemoryStream(contents), fileName, contentType);

            var apiClient = new MediaOptimizationApiClient(_httpClient) { ApiKey = _clientConfiguration.Value.ApiKey };
            var job = await EvaluateResponseAsync(OperationType.MediaOptimization, async () =>
                await apiClient.OptimizeMediaFromFileV2Async(MapImagePlaceholderAlgorithm(imagePlaceholderAlgorithm), file, cancellationToken).ConfigureAwait(false),
                cancellationToken).ConfigureAwait(false);

            await CheckJobStatusAsync(job, cancellationToken).ConfigureAwait(false);
            return await DownloadFileResultAsync(job, cancellationToken).ConfigureAwait(false);
        }
    }

    private static string AdjustFileName(string contentType, string? fileName)
    {
        var extension = Path.GetExtension(fileName);
        return $"f{(!string.IsNullOrWhiteSpace(extension) ? extension : MimeTypeMap.GetExtension(contentType))}";
    }

    private static Activity? StartClientActivity(string activityName, HttpMethod httpMethod, string httpUrl)
    {
        var activity = ThumbnailerClientActivitySource.Instance.StartActivity(activityName, ActivityKind.Client);
        if (activity == null)
        {
            return activity;
        }

        activity.AddTag("http.method", httpMethod.Method);
        activity.AddTag("http.url", httpUrl);
        return activity;
    }

    internal async Task CheckJobStatusAsync(JobDetails job, CancellationToken cancellationToken)
    {
        var stopwatch = Stopwatch.StartNew();
        var endpoint = UrlCombine(UrlCombine(_clientConfiguration.Value.BaseUri.AbsoluteUri, JobsEndpoint), job.JobId);

        while (true)
        {
            using (StartClientActivity(ActivityNames.JobRetrieval, HttpMethod.Get, endpoint))
            {
                var apiClient = new JobApiClient(_httpClient) { ApiKey = _clientConfiguration.Value.ApiKey };
                job = await EvaluateResponseAsync(OperationType.JobRetrieval, async () =>
                    await apiClient.QueryJobV2Async(job.JobId, cancellationToken).ConfigureAwait(false),
                    cancellationToken).ConfigureAwait(false);
            }

            switch (job.Status)
            {
                case JobStatus.Queued:
                case JobStatus.Processing:
                    break;

                case JobStatus.Processed:
                    return;

                case JobStatus.Failed:
                    throw new ThumbnailerClientException(job.FailureReason!, OperationType.JobRetrieval);

                default:
                    throw new ThumbnailerClientException("Async job has an invalid status", OperationType.JobRetrieval);
            }

            if (stopwatch.Elapsed > _clientConfiguration.Value.JobTimeout)
            {
                throw new ThumbnailerClientException("Async job was aborted or timed out", OperationType.JobRetrieval);
            }

            await Task.Delay(500, cancellationToken).ConfigureAwait(false);
        }
    }

    internal async Task<FileResult> DownloadFileResultAsync(JobDetails job, CancellationToken cancellationToken)
    {
        var endpoint = GetDownloadEndpoint(job);
        using (StartClientActivity(ActivityNames.JobResultDownload, HttpMethod.Get, endpoint))
        {
            var apiClient = new JobApiClient(_httpClient) { ApiKey = _clientConfiguration.Value.ApiKey };
            var response = await EvaluateResponseAsync(OperationType.JobRetrieval, async () =>
                await apiClient.DownloadJobResultV2Async(job.JobId, ResponseType.Binary, openInBrowser: false, cancellationToken).ConfigureAwait(false),
                cancellationToken).ConfigureAwait(false);

            string? imagePlaceholder = null;
            if (response.Headers.TryGetValue("X-Image-Placeholder", out var imagePlaceholderHeader))
                imagePlaceholder = imagePlaceholderHeader.FirstOrDefault();

            await using var memoryStream = new MemoryStream();
            await response.Stream.CopyToAsync(memoryStream);
            var contents = memoryStream.ToArray();

            return new FileResult(contents, imagePlaceholder);
        }
    }

    internal async Task<ImagePlaceholderResult> DownloadImagePlaceholderResultAsync(JobDetails job, CancellationToken cancellationToken)
    {
        var endpoint = GetDownloadEndpoint(job);
        using (StartClientActivity(ActivityNames.JobResultDownload, HttpMethod.Get, endpoint))
        {
            var apiClient = new JobApiClient(_httpClient) { ApiKey = _clientConfiguration.Value.ApiKey };
            var response = await EvaluateResponseAsync(OperationType.JobRetrieval, async () =>
                await apiClient.DownloadJobResultV2Async(job.JobId, ResponseType.Binary, openInBrowser: false, cancellationToken).ConfigureAwait(false),
                cancellationToken).ConfigureAwait(false);

            using var streamReader = new StreamReader(response.Stream);
            using var jsonTextReader = new JsonTextReader(streamReader);
            var serializer = JsonSerializer.Create(apiClient.JsonSerializerSettings);
            return serializer.Deserialize<ImagePlaceholderResult>(jsonTextReader) ?? throw new InvalidDataException("Invalid image placeholder result");
        }
    }

    private string GetDownloadEndpoint(JobDetails job)
    {
        return UrlCombine(UrlCombine(_clientConfiguration.Value.BaseUri.AbsoluteUri, JobsEndpoint), $"{job.JobId}/download");
    }

    private async Task<T> EvaluateResponseAsync<T>(
        OperationType operationType, Func<Task<T>> httpCall,
        CancellationToken cancellationToken)
    {
        try
        {
            return await _resiliencePipeline.ExecuteAsync(
                async _ => await httpCall().ConfigureAwait(false),
                cancellationToken).ConfigureAwait(false);
        }
        catch (TaskCanceledException httpTimeoutException) when (httpTimeoutException.InnerException is TimeoutException)
        {
            throw new ThumbnailerClientException("Request was aborted or timed out",
                httpTimeoutException, operationType, default);
        }
        catch (HttpRequestException httpRequestException)
        {
            throw new ThumbnailerClientException("Request failed due to an underlying issue such as network connectivity, DNS failure, server certificate validation",
                httpRequestException, operationType, default);
        }
        catch (ApiException<ProblemDetails> problemDetailsException)
        {
            var message = problemDetailsException.Result.Detail;
            throw new ThumbnailerClientException(message, problemDetailsException, operationType,
                (HttpStatusCode)problemDetailsException.StatusCode);
        }
        catch (ApiException apiException)
        {
            var message = apiException.Response;
            throw new ThumbnailerClientException(message, apiException, operationType,
                (HttpStatusCode)apiException.StatusCode);
        }
    }

    private static Core.ImagePlaceholderAlgorithm MapImagePlaceholderAlgorithm(ImagePlaceholderAlgorithm imagePlaceholderAlgorithm)
    {
        return imagePlaceholderAlgorithm switch
        {
            ImagePlaceholderAlgorithm.BlurHash => Core.ImagePlaceholderAlgorithm.BlurHash,
            ImagePlaceholderAlgorithm.ThumbHash => Core.ImagePlaceholderAlgorithm.ThumbHash,
            _ => Core.ImagePlaceholderAlgorithm.None,
        };
    }

    private static string UrlCombine(string url1, string url2)
    {
        if (url1.Length == 0)
        {
            return url2;
        }

        if (url2.Length == 0)
        {
            return url1;
        }

        url1 = url1.TrimEnd('/', '\\');
        url2 = url2.TrimStart('/', '\\');

        return string.Format("{0}/{1}", url1, url2);
    }

    #region IDisposable members

    /// <inheritdoc/>
    public void Dispose()
    {
        // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method.
        Dispose(disposing: true);
        GC.SuppressFinalize(this);
    }

    private void Dispose(bool disposing)
    {
        if (!_disposedValue)
        {
            if (disposing)
            {
                _httpClient.Dispose();
            }
            _disposedValue = true;
        }
    }

    #endregion IDisposable members
}
