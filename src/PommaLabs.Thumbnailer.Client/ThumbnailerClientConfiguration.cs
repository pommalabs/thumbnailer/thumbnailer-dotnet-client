﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;

namespace PommaLabs.Thumbnailer.Client;

/// <summary>
///   Thumbnailer client configuration.
/// </summary>
public sealed class ThumbnailerClientConfiguration
{
    private string _apiKey = string.Empty;
    private Uri _baseUri = new("http://localhost:8080");
    private TimeSpan _jobTimeout = TimeSpan.FromMinutes(10);
    private int _retryCount = 2;
    private TimeSpan _timeout = TimeSpan.FromSeconds(30);

    /// <summary>
    ///   Optional, but suggested, API key.
    /// </summary>
    public string ApiKey
    {
        get => _apiKey;
        set => _apiKey = string.IsNullOrWhiteSpace(value) ? _apiKey : value;
    }

    /// <summary>
    ///   Base service URI. Defaults to "http://localhost:8080".
    /// </summary>
    public Uri BaseUri
    {
        get => _baseUri;
        set => _baseUri = value ?? throw new ArgumentNullException(nameof(value), "Base URI cannot be null");
    }

    /// <summary>
    ///   How long an async job can last before being interrupted. Defaults to 10 minutes and it
    ///   cannot be greater than one hour.
    /// </summary>
    public TimeSpan JobTimeout
    {
        get => _jobTimeout;
        set => _jobTimeout = (value > TimeSpan.FromMinutes(60))
            ? throw new ArgumentOutOfRangeException(nameof(value), "Timeout must be less than or equal to one hour")
            : value;
    }

    /// <summary>
    ///   How many times the client will attempt to perform again HTTP calls which are worth
    ///   retrying. Defaults to 2 times.
    /// </summary>
    public int RetryCount
    {
        get => _retryCount;
        set => _retryCount = (value < 0)
            ? throw new ArgumentOutOfRangeException(nameof(value), "Retry count must be greater than or equal to zero")
            : value;
    }

    /// <summary>
    ///   How long a single HTTP call can last before being interrupted. Defaults to 30 seconds and
    ///   it cannot be greater than 10 minutes.
    /// </summary>
    public TimeSpan Timeout
    {
        get => _timeout;
        set => _timeout = (value > TimeSpan.FromMinutes(10))
            ? throw new ArgumentOutOfRangeException(nameof(value), "Timeout must be less than or equal to ten minutes")
            : value;
    }
}
