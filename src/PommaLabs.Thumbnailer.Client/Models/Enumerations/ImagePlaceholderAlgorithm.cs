﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Thumbnailer.Client.Models.Enumerations;

/// <summary>
///   Image placeholder algorithms list.
/// </summary>
public enum ImagePlaceholderAlgorithm
{
    /// <summary>
    ///   No placeholder image will be generated.
    /// </summary>
    None = 0,

    /// <summary>
    ///   Use the BlurHash algorithm to generate the placeholder image.
    /// </summary>
    BlurHash = 1,

    /// <summary>
    ///   Use the ThumbHash algorithm to generate the placeholder image.
    /// </summary>
    ThumbHash = 2,
}
