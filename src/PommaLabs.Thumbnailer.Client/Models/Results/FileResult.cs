﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Thumbnailer.Client.Models.Results;

/// <summary>
///   Represents a file generated as the result of a Thumbnailer operation.
/// </summary>
/// <param name="contents">File bytes.</param>
/// <param name="imagePlaceholder">
///   A very compact representation of a placeholder for an image,
///   if requested and available.
/// </param>
public sealed class FileResult(byte[] contents, string? imagePlaceholder)
{
    /// <summary>
    ///   File bytes.
    /// </summary>
    public byte[] Contents => contents;

    /// <summary>
    ///   A very compact representation of a placeholder for an image,
    ///   if requested and available.
    /// </summary>
    public string? ImagePlaceholder => imagePlaceholder;
}
