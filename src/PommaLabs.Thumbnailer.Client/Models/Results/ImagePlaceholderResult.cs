﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Thumbnailer.Client.Models.Results;

/// <summary>
///   Image placeholder generation operation result.
/// </summary>
/// <param name="placeholder">
///   A very compact representation of a placeholder for an image.
/// </param>
public sealed class ImagePlaceholderResult(string placeholder)
{
    /// <summary>
    ///   A very compact representation of a placeholder for an image.
    /// </summary>
    public string Placeholder => placeholder;
}
