﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Runtime.CompilerServices;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using MsHealthStatus = Microsoft.Extensions.Diagnostics.HealthChecks.HealthStatus;

namespace PommaLabs.Thumbnailer.Client.Core;

/// <summary>
///   Utility class which handles caching of health check results.
/// </summary>
/// <typeparam name="TService">
///   The type of the service which is the subject of the health check.
/// </typeparam>
/// <remarks>
///   This class implements a caching mechanism which works only with services registered as singletons.
/// </remarks>
internal static class HealthCheckCacheHelper<TService> where TService : class
{
    private static readonly ConditionalWeakTable<TService, CachedResult> s_cachedResults = new();

    /// <summary>
    ///   Adds given health check result to cache.
    /// </summary>
    /// <param name="service">The service which is the subject of the health check.</param>
    /// <param name="result">The result of the health check.</param>
    public static void AddResultToCache(TService service, HealthCheckResult result)
    {
        var cacheLifetime = TimeSpan.FromSeconds(result.Status == MsHealthStatus.Healthy ? 27 : 9);
        s_cachedResults.AddOrUpdate(service, new CachedResult(result, DateTime.UtcNow + cacheLifetime));
    }

    /// <summary>
    ///   Removes all cached results.
    /// </summary>
    public static void ClearCachedResults()
    {
        s_cachedResults.Clear();
    }

    /// <summary>
    ///   Tries to retrieve a cached health check result.
    /// </summary>
    /// <param name="service">The service which is the subject of the health check.</param>
    /// <param name="result">The cached result of the health check, if available.</param>
    /// <returns>True if a cached health check result is available, false otherwise.</returns>
    public static bool TryGetCachedResult(TService service, out HealthCheckResult result)
    {
        if (s_cachedResults.TryGetValue(service, out var cachedResult) && DateTime.UtcNow < cachedResult.ExpiresAt)
        {
            result = cachedResult.Result;
            return true;
        }
        result = default;
        return false;
    }

    private sealed class CachedResult(HealthCheckResult result, DateTime expiresAt)
    {
        public HealthCheckResult Result => result;

        public DateTime ExpiresAt => expiresAt;
    }
}
