﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;

namespace PommaLabs.Thumbnailer.Client.Core;

internal abstract class ApiClientBase
{
    public string? ApiKey { get; set; }

    protected Task<HttpRequestMessage> CreateHttpRequestMessageAsync(CancellationToken _)
    {
        var request = new HttpRequestMessage();
        if (!string.IsNullOrWhiteSpace(ApiKey))
        {
            request.Headers.Authorization = new AuthenticationHeaderValue("X-Api-Key", ApiKey);
        }
        return Task.FromResult(request);
    }
}
