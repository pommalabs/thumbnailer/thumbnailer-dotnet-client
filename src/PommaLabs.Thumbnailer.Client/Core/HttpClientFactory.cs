﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Net.Http;
using Microsoft.Extensions.Options;

namespace PommaLabs.Thumbnailer.Client.Core;

internal static class HttpClientFactory
{
    public static Func<IOptions<ThumbnailerClientConfiguration>, HttpClient> CreateHttpClient { get; set; } = clientConfiguration =>
    {
        return new HttpClient
        {
            BaseAddress = clientConfiguration.Value.BaseUri,
            Timeout = clientConfiguration.Value.Timeout,
        };
    };
}
