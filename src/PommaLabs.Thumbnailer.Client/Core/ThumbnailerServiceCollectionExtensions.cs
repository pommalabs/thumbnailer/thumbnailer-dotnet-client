﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using Microsoft.Extensions.DependencyInjection.Extensions;
using PommaLabs.Thumbnailer.Client;

namespace Microsoft.Extensions.DependencyInjection;

/// <summary>
///   Registrations of <see cref="IThumbnailerClient"/> implementations.
/// </summary>
public static class ThumbnailerServiceCollectionExtensions
{
    /// <summary>
    ///   Registers <see cref="StandardThumbnailerClient"/> as singleton implementation of <see cref="IThumbnailerClient"/>.
    /// </summary>
    /// <param name="services">Service collection.</param>
    /// <returns>Modified service collection.</returns>
    public static IServiceCollection AddThumbnailerClient(this IServiceCollection services)
    {
        services.TryAddSingleton<IThumbnailerClient, StandardThumbnailerClient>();
        return services;
    }
}
