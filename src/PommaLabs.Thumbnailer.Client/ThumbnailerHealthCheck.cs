﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using PommaLabs.Thumbnailer.Client.Core;

namespace PommaLabs.Thumbnailer.Client;

/// <summary>
///   Health check for Thumbnailer service.
/// </summary>
/// <param name="thumbnailerClient">Thumbnailer client.</param>
public sealed class ThumbnailerHealthCheck(IThumbnailerClient thumbnailerClient) : IHealthCheck
{
    /// <inheritdoc/>
    public async Task<HealthCheckResult> CheckHealthAsync(
        HealthCheckContext context, CancellationToken cancellationToken = default)
    {
        if (HealthCheckCacheHelper<IThumbnailerClient>.TryGetCachedResult(thumbnailerClient, out var result))
        {
            return result;
        }
        var isHealthy = await thumbnailerClient.IsHealthyAsync(cancellationToken).ConfigureAwait(false);
        result = isHealthy ? HealthCheckResult.Healthy() : HealthCheckResult.Unhealthy();
        HealthCheckCacheHelper<IThumbnailerClient>.AddResultToCache(thumbnailerClient, result);
        return result;
    }
}
