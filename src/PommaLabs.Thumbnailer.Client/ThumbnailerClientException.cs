﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Net;
using PommaLabs.Thumbnailer.Client.Models.Enumerations;

namespace PommaLabs.Thumbnailer.Client;

/// <summary>
///   Represents an error produced by Thumbnailer service.
/// </summary>
public class ThumbnailerClientException : Exception
{
    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="message">Message.</param>
    /// <param name="operationType">Operation type.</param>
    public ThumbnailerClientException(
        string message, OperationType operationType)
        : base(message)
    {
        Data[nameof(OperationType)] = operationType;
    }

    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="message">Message.</param>
    /// <param name="innerException">Inner exception.</param>
    /// <param name="operationType">Operation type.</param>
    /// <param name="httpStatusCode">HTTP status code.</param>
    public ThumbnailerClientException(
        string message, Exception innerException,
        OperationType operationType, HttpStatusCode? httpStatusCode)
        : base(message, innerException)
    {
        Data[nameof(OperationType)] = operationType;
        Data[nameof(HttpStatusCode)] = httpStatusCode;
    }

    /// <summary>
    ///   Constructor.
    /// </summary>
    public ThumbnailerClientException()
    {
    }

    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="message">Message</param>
    public ThumbnailerClientException(string message)
        : base(message)
    {
    }

    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="message">Message.</param>
    /// <param name="innerException">Inner exception.</param>
    public ThumbnailerClientException(string message, Exception innerException)
        : base(message, innerException)
    {
    }

    /// <summary>
    ///   HTTP status code.
    /// </summary>
    public HttpStatusCode? HttpStatusCode => Data[nameof(HttpStatusCode)] as HttpStatusCode?;

    /// <summary>
    ///   Operation type.
    /// </summary>
    public OperationType? OperationType => Data[nameof(OperationType)] as OperationType?;
}
